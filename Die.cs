﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

class Die
{
    private int numberOfSides;
    //private Random randomGenerator; //drugi zadatak
    private RandomGenerator randomGenerator;

    //public Die(int numberOfSides) //konstruktor za prvi zadatak
    //{
    //    this.numberOfSides = numberOfSides;
    //}
    
    // public Die(int numberOfSides, Random randomGenerator)// drugi zadatak 
    public Die(int numberOfSides)
    {
        this.numberOfSides = numberOfSides;
        //this.randomGenerator = new Random();      
        this.randomGenerator = RandomGenerator.GetInstance();
    }
    public int Roll()
    {
        //int rolledNumber = randomGenerator.Next(1, numberOfSides + 1); //drugi zadatak
        //return rolledNumber;
        return this.randomGenerator.NextInt(1, numberOfSides + 1);
    }
}

