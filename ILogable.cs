﻿using System;

interface ILogable
{
    string GetStringRepresentation();
}
