﻿using System;

class FileLogger : ILogger
{
    private string filePath;
    public FileLogger(string filePath)
    {
        this.filePath = filePath;
    }
    //public void Log(string message) //4ti zadatak
    public void Log(ILogable data)
    {
        using (System.IO.StreamWriter fileWriter =
       new System.IO.StreamWriter(this.filePath, true))
        {
          // fileWriter.WriteLine(message); //4ti zadatak
          fileWriter.WriteLine(data.GetStringRepresentation());
        }
    }
}