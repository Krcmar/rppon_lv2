﻿using System;

interface IRoller
{
    void RollAllDice();
}

interface IHandleDice
{
    void RemoveAllDice();
    void RollAllDice();
}
